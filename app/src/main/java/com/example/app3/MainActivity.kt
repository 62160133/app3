package com.example.app3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import com.example.app3.adapter.OilAdapter
import com.example.app3.data.Datasource

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val myDataset = Datasource().loadData()
        val recycleView = findViewById<RecyclerView>(R.id.recycleView)
        recycleView.adapter = OilAdapter(this,myDataset)
        recycleView.setHasFixedSize(true)
    }
}